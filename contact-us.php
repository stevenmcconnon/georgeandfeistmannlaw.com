<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Contact Us</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<style  type="text/css">
  .text-label {
    color: #333;
    font-weight: bold;
}

</style>
<script src="js/jquery.validate.js" type="text/javascript"></script>
<script>
$.validator.addMethod('requiredDefault', function(value, element, param) {
        return value && value != param; // Compare with blank and default (parameter) value
    },
    'Please enter a value.');

$(function() { // Shorthand for $(document).ready(function() {
      $('#reach').validate({
            rules: {
                  input4: { requiredDefault: 'Name' },
                  input: { requiredDefault: 'Email', email: true }
                  
            }
      });
});
</script>
      <style type="text/css">

#reach label.error {
	
	float:right;
	
	clear:both;
	width: auto;
	
	color: #c00;
}

    </style>
	
</head>

<body>
<?php require_once("inc/header.php"); ?>
<div id="wrapper">

  <div id="banner"></div>
  
    
  
   <div id="gold"></div>
  
  <div id="main">
   

  <div class="clearfix"></div>
<?php require_once("inc/sidebar.php"); ?>
   
    <div id="content">
      <h1>Contact Us</h1>
      <br/>
      <p>George &amp; Feistmann, P.A<br>
2161 Palm Beach Lakes Blvd., Ste 217<br>
West Palm Beach, Florida 33409<br>
tel: 561 659 6599<br>
cell: 561 420 3749/ 561 714 3817<br>
fax: 561 659 1118<br>
email: <a href="mailto: ettie@palmbeachtrialgroup.com">ettie@palmbeachtrialgroup.com</a></p>

    </div>
    
    
  </div>
</div>

<div class="clearfix"></div>


<?php require_once("inc/footer.php"); ?>

<SCRIPT>

$('input[type="text"]').each(function(){



	this.value = $(this).attr('title');

	$(this).addClass('text-label');



	$(this).focus(function(){

		if(this.value == $(this).attr('title')) {

			this.value = '';

			$(this).removeClass('text-label');

		}

	});



	$(this).blur(function(){

		if(this.value == '') {

			this.value = $(this).attr('title');

			$(this).addClass('text-label');

		}

	});

});



</SCRIPT>
</body>
</html>
