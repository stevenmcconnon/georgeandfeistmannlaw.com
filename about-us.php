<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>About Our Lawers</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery.js">
</script>
    <style type="text/css">
/*<![CDATA[*/
    .text-label {
    color: #333;
    font-weight: bold;
    }

    /*]]>*/
    </style>
    <script src="js/jquery.validate.js" type="text/javascript">
</script>
    <script type="javascript">
<![CDATA[
    $.validator.addMethod('requiredDefault', function(value, element, param) {
        return value && value != param; // Compare with blank and default (parameter) value
    },
    'Please enter a value.');

    $(function() { // Shorthand for $(document).ready(function() {
      $('#reach').validate({
            rules: {
                  input4: { requiredDefault: 'Name' },
                  input: { requiredDefault: 'Email', email: true }
                  
            }
      });
    });
    ]]>
    </script>
    <style type="text/css">
/*<![CDATA[*/

    #reach label.error {

    float:right;

    clear:both;
    width: auto;

    color: #c00;
    }

    /*]]>*/
    </style>
</head>

<body>
    <?php require_once("inc/header.php"); ?>

    <div id="wrapper">
        <div id="banner"></div>

        <div id="gold"></div>

        <div id="main">
            <div class="clearfix"></div><?php require_once("inc/sidebar.php"); ?>

            <div id="content">
                <h1>Our Lawers</h1>

                <div style="margin: 0 auto; text-align:center;">
                    <div style="float:left; width:340px;">
                        <a href="ettie_feistmann.php"><img src="images/GetAttachment.jpg" border="1" height="200" width="150" alt="" /><br />
                        Ettie Feistmann, Esq.
                        <br/>
                        <button>Click For Bio</button>
                        </a>
                    </div>

                    <div style="float:left; width:340px;">
                        <img src="images/shade.png" border="1" height="200" width="150" alt="" /><br />
                        George
                    </div>
                </div>
                
                <p>&nbsp;</p>
                <p>Our law firm is dedicated to serve your needs during the litigation period. Our loyal, experienced and dedicated lawyers and staff understand the anxiety involved in litigating a case, and thus they are committed to give you personalized attention and support. With our extensive experience and tenacious devotion we will do everything within our power to giving you the best result.</p>
            </div>
        </div>
    </div>

    <div class="clearfix"></div><?php require_once("inc/footer.php"); ?><script type="javascript">
<![CDATA[

    $('input[type="text"]').each(function(){



    this.value = $(this).attr('title');

    $(this).addClass('text-label');



    $(this).focus(function(){

        if(this.value == $(this).attr('title')) {

            this.value = '';

            $(this).removeClass('text-label');

        }

    });



    $(this).blur(function(){

        if(this.value == '') {

            this.value = $(this).attr('title');

            $(this).addClass('text-label');

        }

    });

    });



    ]]>
    </script>
</body>
</html>
