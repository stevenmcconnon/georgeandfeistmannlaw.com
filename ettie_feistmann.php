<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>About Our Lawers</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery.js">
</script>
    <style type="text/css">
/*<![CDATA[*/
    .text-label {
    color: #333;
    font-weight: bold;
    }

    /*]]>*/
    </style>
    <script src="js/jquery.validate.js" type="text/javascript">
</script>
    <script type="text/javascript">
//<![CDATA[
    $.validator.addMethod('requiredDefault', function(value, element, param) {
        return value && value != param; // Compare with blank and default (parameter) value
    },
    'Please enter a value.');

    $(function() { // Shorthand for $(document).ready(function() {
      $('#reach').validate({
            rules: {
                  input4: { requiredDefault: 'Name' },
                  input: { requiredDefault: 'Email', email: true }
                  
            }
      });
    });
    //]]>
    </script>
    <style type="text/css">
/*<![CDATA[*/

    #reach label.error {

    float:right;

    clear:both;
    width: auto;

    color: #c00;
    }

    /*]]>*/
    </style>
</head>

<body>
    <?php require_once("inc/header.php"); ?>

    <div id="wrapper">
        <div id="banner"></div>

        <div id="gold"></div>

        <div id="main">
            <div class="clearfix"></div><?php require_once("inc/sidebar.php"); ?>

            <div id="content">
                <h1>Ettie Feistmann</h1>

                <p><strong>Practice Area(s)</strong><br />
                Criminal Defense, Family Law, Personal Injury and Wrongful Death<br />
                <br />
                <strong>Biography</strong></p>

                <p>Attorney Feistmann has over 18 years of experience as a state prosecutor, including 5 years as the division chief in felony. She has been the lead attorney in over 200 jury trials of major crimes including homicide, fraud, crimes against children and elderly, and drug related cases. Her almost two decades of experience on the prosecution side has been invaluable to her current practice as a defense attorney.</p>

                <p>Most recently, Attorney Feistmann has successfully defended serious felony cases, including armed burglary with a firearm, grand theft from a dwelling, DUI, drug related cases, drug trafficking and unlawful prescription of controlled substances.</p>

                <p>Furthermore, Attorney Feistmann has negotiated multiple personal injury cases, including car accidents and slip and fall.</p>

                <p>Attorney Feistmann has extensive experience as an appellate attorney, as well, having handled over 400 appeals for the state.</p>

                <p>Attorney Feistmann earned her law degree from Nova Southeastern University. Prior to that, she earned an MBA and computer science degree from Florida Atlantic University.</p>

                <p>Attorney Feistmann was born and raised in Israel where she served in the IDF military, from which she was honorably discharged as a corporal. She currently resides in West Palm Beach.</p><br />
                <br />

                <p><strong>Bar Admissions</strong><br /></p>

                <ul>
                    <li>The Florida Bar 1991</li>

                    <li>The United State District Court Southern District of Florida 1992</li>

                    <li>The United States Supreme Court 2011</li>

                    <li style="list-style: none; display: inline">
                        <p><strong>Education and Degrees Earned</strong><br /></p>

                        <ul>
                            <li>Nova Southeastern Law School, Fort Lauderdale, Florida, Juris Doctor(JD)</li>

                            <li>Florida Atlantic University Boca Raton, Florida Master’s of Business Administration (MBA)</li>

                            <li>Florida Atlantic University Boca Raton, Florida, Bachelor’s of Science (BS)</li>
                        </ul>

                        <p></p>

                        <p><strong>Martindale rating:</strong><br />
                        AV Rated</p>

                        <p><strong>Military Service</strong>:<br />
                        Served in the Israeli Defense Force (IDF)<br />
                        Honorable discharge, Corporal ranking<br />
                        <br />
                        <strong>Languages</strong>:<br />
                        Fluent in English, Hebrew and Spanish</p>

                        <p><strong>Affiliations and Committees</strong><br />
                        <strong>Membership:&nbsp;</strong><br /></p>

                        <ul>
                            <li>Palm Beach County Bar Association, Member (2009-present)</li>

                            <li>Palm Beach County Justice Association, Member (2012)</li>

                            <li>Palm Beach Association of Criminal Defense, Member (2012)</li>

                            <li>Professional Ethics Committee, Member (2009/2011)</li>

                            <li>Judicial Relations Committee, Member (2009-present)</li>

                            <li>Membership Committee, Member( 2009-present)</li>

                            <li>Diversity Committee, Member (2011/2012)</li>

                            <li>Bench Bar Committee, Member (2009-present)</li>

                            <li>Criminal Practice Committee, Chair (2011/2012)</li>

                            <li>F. Malcolm Cunningham, Sr. Bar Association, Member (2009-present)</li>

                            <li>Hispanic Bar Association, Member (2009-present)</li>

                            <li>Voters’ Coalition, Member (2009-present)</li>

                            <li>League of Women Voters of Palm Beach County, Member(2009-present)</li>

                            <li>On the Board of Directors (2011/2012)</li>

                            <li>Advocacy/Education Subcommittee, Leadership Liaison (2009/2010)</li>

                            <li>Membership Subcommittee, Member (2009/2010)</li>

                            <li>Craig S. Barnard American Inn of Court, Member at Large (2009-present)</li>

                            <li>On the Board of Trustees</li>

                            <li>Group Leader(2011-present)</li>

                            <li>Executive Women of the Palm Beaches, Member (2009-present)</li>

                            <li>Women In Leaderships Award (WILA) Subcommittee, Member(2009-present)</li>

                            <li>Advocacy Subcommittee (2011 to present); Planning Subcommittee (2011-present)</li>

                            <li>Florida Association for Women Lawyers (FAWL), Member(2009-present)</li>

                            <li>Mentoring Subcommittee, Member (2011/2012)</li>

                            <li>National Organization for Women (NOW), Member(2009-present)</li>

                            <li>The Federal Bar, Palm Beach County Chapter, Member</li>

                            <li>Downtown Neighborhood Association (DNA), Member (2009-present)</li>

                            <li>Social Subcommittee, Member</li>

                            <li>Hadassah, Palm Beach Chapter, Life Member (since 2009)</li>

                            <li>Deborah Hospital Foundation, Life Member(since 2009)</li>
                        </ul>

                        <p></p>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="clearfix"></div><?php require_once("inc/footer.php"); ?><script type="text/javascript">
//<![CDATA[

    $('input[type="text"]').each(function(){



    this.value = $(this).attr('title');

    $(this).addClass('text-label');



    $(this).focus(function(){

        if(this.value == $(this).attr('title')) {

            this.value = '';

            $(this).removeClass('text-label');

        }

    });



    $(this).blur(function(){

        if(this.value == '') {

            this.value = $(this).attr('title');

            $(this).addClass('text-label');

        }

    });

    });



    //]]>
    </script>
</body>
</html>
