<?php
# ----------------------------------------------------
# -----
# ----- 
# -----
# ----------------------------------------------------

if(isset($_POST['Submit']))
error_reporting(7);


if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
$ClientIP = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
$ClientIP = $_SERVER['REMOTE_ADDR'];
}


$FTGmail = $_POST['input4'];
$FTGmail1 = $_POST['input'];
$FTGmail2 = $_POST['input2'];
$FTGmail3 = $_POST['input3'];


$FTGupload = $_FILES['upload']['name'];


if (get_magic_quotes_gpc) {
$FTGmail = stripslashes($FTGmail);
}
# Redirect user to the error page

if ($validationFailed == true) {

header("Location: error.html");
exit;

}

# Email to Form Owner

$emailTo = '"Contact" <ettie@palmbeachtrialgroup.com>';

$emailSubject = "Contact form from $FTGmail";
$emailSubject = preg_replace('/[\x00-\x1F]/', '', $emailSubject);

$emailFrom = $FTGmail1;
$emailFrom = preg_replace('/[\x00-\x1F]/', '', $emailFrom);

$emailHeader = "From: $emailFrom\n"
. "Reply-To: $emailFrom\n"
. "MIME-Version: 1.0\n"
. "Content-Type: multipart/mixed; boundary=\"FTG_BOUNDRY\"\n"
. "X-Sender: $emailFrom\n"
. "X-Mailer: PHP\n"
. "X-Priority: 3\n"
. "Return-Path: $emailFrom\n"
. "This is a multi-part Content MIME format.\n";

$emailBody = "--FTG_BOUNDRY\n"
. "Content-Type: text/plain; charset=\"ISO-8859-1\"\n"
. "Content-Transfer-Encoding: quoted-printable\n"


. "\n"
. "Name: $FTGmail\n"
. "\n"
. "Email: $FTGmail1\n"
. "\n"
. "Phone Number: $FTGmail2\n"
. "\n"
. "Message: $FTGmail3\n"
. "\n"

. "\n";

if (file_exists($_FILES['upload']['tmp_name']) === true) {

$fileName = $_FILES['upload']['tmp_name'];
$fileHandle = fopen($fileName, 'r');
$fileAttach = fread($fileHandle, filesize ($fileName));
fclose($fileHandle);

$fileAttach = chunk_split(base64_encode($fileAttach));

$emailBody .= "--FTG_BOUNDRY\n"
 . "Content-Type: " . $_FILES['upload']['type'] . "; name=\"" . $_FILES['upload']['name'] . "\"\n"
 . "Content-disposition: attachment\n"
 . "Content-transfer-encoding: base64\n"
 . "\n"
 . "$fileAttach\n"
 . "\n";

}

$emailBody .= "--FTG_BOUNDRY--\n";

mail($emailTo, $emailSubject, $emailBody, $emailHeader);

?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>Thank You</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery.js">
</script>
    <style type="text/css">
/*<![CDATA[*/
    .text-label {
    color: #333;
    font-weight: bold;
    }

    /*]]>*/
    </style>
    <script src="js/jquery.validate.js" type="text/javascript">
</script>
    <script type="javascript">
<![CDATA[
    $.validator.addMethod('requiredDefault', function(value, element, param) {
        return value && value != param; // Compare with blank and default (parameter) value
    },
    'Please enter a value.');

    $(function() { // Shorthand for $(document).ready(function() {
      $('#reach').validate({
            rules: {
                  input4: { requiredDefault: 'Name' },
                  input: { requiredDefault: 'Email', email: true }
                  
            }
      });
    });
    ]]>
    </script>
    <style type="text/css">
/*<![CDATA[*/

    #reach label.error {

    float:right;

    clear:both;
    width: auto;

    color: #c00;
    }

    /*]]>*/
    </style>
</head>

<body>
    <?php require_once("header.php"); ?>

    <div id="wrapper">
        <div id="banner"></div>

        <div id="gold"></div>

        <div id="main">
            <div class="clearfix"></div><?php require_once("sidebar.php"); ?>

            <div id="content">
                <h1>Thanks</h1>

<p>We will be in touch with you shortly.</p>
            </div>
        </div>
    </div>

    <div class="clearfix"></div><?php require_once("footer.php"); ?><script type="javascript">
<![CDATA[

    $('input[type="text"]').each(function(){



    this.value = $(this).attr('title');

    $(this).addClass('text-label');



    $(this).focus(function(){

        if(this.value == $(this).attr('title')) {

            this.value = '';

            $(this).removeClass('text-label');

        }

    });



    $(this).blur(function(){

        if(this.value == '') {

            this.value = $(this).attr('title');

            $(this).addClass('text-label');

        }

    });

    });



    ]]>
    </script>
</body>
</html>
