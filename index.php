<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome to George and Feistmann Law</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<style  type="text/css">
  .text-label {
    color: #333;
    font-weight: bold;
}

</style>
<script src="js/jquery.validate.js" type="text/javascript"></script>
<script>
$.validator.addMethod('requiredDefault', function(value, element, param) {
        return value && value != param; // Compare with blank and default (parameter) value
    },
    'Please enter a value.');

$(function() { // Shorthand for $(document).ready(function() {
      $('#reach').validate({
            rules: {
                  input4: { requiredDefault: 'Name' },
                  input: { requiredDefault: 'Email', email: true }
                  
            }
      });
});
</script>
      <style type="text/css">

#reach label.error {
	
	float:right;
	
	clear:both;
	width: auto;
	
	color: #c00;
}

    </style>
	
</head>

<body>
<?php require_once("inc/header.php"); ?>
<div id="wrapper">

  <div id="banner"></div>
  
    
  
   <div id="gold"></div>
  
  <div id="main">
   

  <div class="clearfix"></div>
<?php require_once("inc/sidebar.php"); ?>
   
    <div id="content">
      <h1>George and Feistmann Law Firm</h1>
      <br/>
      <p>With over 30 years combined experience as former prosecutors in trial and appeals, we will handle your Criminal, Family Law, and Personal Injury cases. Our attorneys have handled and negotiated thousands of cases, tried over 250 criminal trials and appeals.  Our attorneys are aggressive but compassionate.  We understand your needs and will reduce your anxiety during litigation and seek justice.</p>
      <p>We understand that you may experience anxiety and stress during litigation.  That is why we pride ourselves on giving you full attention to your personal needs.  Our attorneys provide aggressive and thorough legal representation to anyone who is accused of any criminal charges, injured in any accident, or has matrimonial issues.  We have extensive experience in trials and appeals in State and Federal courts.  Our attorneys are experienced and able to either negotiate or try your case.</p>  

<p>We understand that your case is very important to you and therefore we will give you our most attention and make sure you are getting the best representation for your case. Our dedication, skills, knowledge, experience and passion for justice will help you obtain the best results in court.</p>   

<p>Our legal team includes lawyers, investigators, paralegals and translators. We pride ourselves for being dedicated and attentive to your needs. Our team can handle the pretrial litigation, trial, and all post-conviction litigations, including appeals. We are going to follow your needs from the moment of accusation all the way to the Supreme Court.</p>

<p>Choosing a loyal, tenacious, dedicated, experienced, and aggressive attorney is probably the most important decision you will ever make about your case.</p>
<p>
<b>You may reach us at any time and you will be able to speak to one of our attorneys.  </b></p>

    </div>
    
    
  </div>
</div>

<div class="clearfix"></div>


<?php require_once("inc/footer.php"); ?>


<SCRIPT>

$('input[type="text"]').each(function(){



	this.value = $(this).attr('title');

	$(this).addClass('text-label');



	$(this).focus(function(){

		if(this.value == $(this).attr('title')) {

			this.value = '';

			$(this).removeClass('text-label');

		}

	});



	$(this).blur(function(){

		if(this.value == '') {

			this.value = $(this).attr('title');

			$(this).addClass('text-label');

		}

	});

});



</SCRIPT>
</body>
</html>
