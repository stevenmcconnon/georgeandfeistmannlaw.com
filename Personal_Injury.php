<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>About Our Lawers</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery.js">
</script>
    <style type="text/css">
/*<![CDATA[*/
    .text-label {
    color: #333;
    font-weight: bold;
    }

    /*]]>*/
    </style>
    <script src="js/jquery.validate.js" type="text/javascript">
</script>
    <script type="javascript">
<![CDATA[
    $.validator.addMethod('requiredDefault', function(value, element, param) {
        return value && value != param; // Compare with blank and default (parameter) value
    },
    'Please enter a value.');

    $(function() { // Shorthand for $(document).ready(function() {
      $('#reach').validate({
            rules: {
                  input4: { requiredDefault: 'Name' },
                  input: { requiredDefault: 'Email', email: true }
                  
            }
      });
    });
    ]]>
    </script>
    <style type="text/css">
/*<![CDATA[*/

    #reach label.error {

    float:right;

    clear:both;
    width: auto;

    color: #c00;
    }

    /*]]>*/
    </style>
</head>

<body>
    <?php require_once("inc/header.php"); ?>

    <div id="wrapper">
        <div id="banner"></div>

        <div id="gold"></div>

        <div id="main">
            <div class="clearfix"></div><?php require_once("inc/sidebar.php"); ?>

            <div id="content">
                <h1 style="font-size: 35px;">Personal Injury and Accident Attorneys</h1>

          <br/>
<b>WE HANDLE AUTO ACCIDENT, MEDICAL MALPRACTICE AND PRODUCT LIABILITY CASES THROUGHOUT FLORIDA </b>
<br/><br/>
<p>When you are injured by another party’s failure to take reasonable care to avoid the accident, you may have the right to be compensated for your injuries.  Our experienced team is dedicated to providing effective legal representation to those who suffer injury caused by the careless or intentional misconduct of a third party.  Our law firm is dedicated to helping you when you suffer from injury or death caused by the negligence of another person or by a defective product.  Our law firm has successfully handled thousands of personal injury cases over the last decade.  </p>

<br/>
<b>Cases We Handle</b>
<ul>
<li>Auto Accidents</li>
<li>Truck Accidents</li>
<li>Motorcycle Accidents</li>
<li>DUI Accidents</li>
<li>Bike Accidents</li>
<li>Rental Truck Accidents</li>
<li>Uninsured Motorists</li>
<li>ATV Accidents</li>
<li>Railroad Accidents</li>
<li>Boating Accidents</li>
<li>Bus Accidents</li>
<li>Pedestrian Accidents</li>
<li>Aviation Accidents</li>
<li>Cruise Ship Accidents</li>
<li>Elevator and Escalator Accidents</li>
<li>Unsecure Truck Loads</li>
</ul>
<br/><br/>

<b>Products Liability</b>
<ul>
<li>Tire Failures</li>
<li>Vehicle Rollover</li>
<li>Seat Belt Failures</li>
<li>Airbag Defects</li>
<li>Vehicle Roof Collapse</li>
<li>Vehicle Fires</li>
<li>15-Passenger Vans</li>
<li>Child Safety Seats</li>
<li>Medical Devices</li>
<li>Pharmaceuticals</li>
</ul>
<br/><br/>

<b>Additional Practice Areas</b>
<li>Wrongful Death</li>
<li>Nursing Home Negligence</li>
<li>Insurance Bad Faith</li>
<li>Negligent Security</li>
<li>Environmental Toxic Torts</li>
<li>Mass Torts</li>
<li>Slip and Fall</li>
<li>Spinal Cord Injury</li>
<li>Dog Bites</li>
<li>Construction Site Accidents</li>
<li>Gulf Oil Spill</li>
<li>Appellate Litigation</li>
<li>Traumatic Brain Injury</li>
<li>Speeding drivers</li>
<li>Motorcycle collisions</li>
<li>Sideswipe impacts</li>
<li>Driveway/parking lot backup accidents</li>
<li>Pileups involving multiple cars</li>
<li>DUI/DWI crashes</li>
<li>Collisions involving commuter, long-distance, tour and school buses</li>
<li>Cars hitting pedestrians</li>
<li>Crashes resulting in injuries caused by airbags</li>
<li>Tractor-trailer accidents</li>
<li>Crushed roof rollovers</li>
<li>Collisions with uninsured/underinsured motorists (UM)</li>
<li>Rear-enders</li>
</ul>

<br/>
<p align=center><b>
You owe us nothing until we win<br/>
No Recovery, No Fee, No Costs<br/>
Free Initial Consultation<br/>
We are available 24/7 at any time <br/>
We speak Spanish too 
</b>
</p>

          
          
          
          
            </div>
        </div>
    </div>

    <div class="clearfix"></div><?php require_once("inc/footer.php"); ?><script type="javascript">
<![CDATA[

    $('input[type="text"]').each(function(){



    this.value = $(this).attr('title');

    $(this).addClass('text-label');



    $(this).focus(function(){

        if(this.value == $(this).attr('title')) {

            this.value = '';

            $(this).removeClass('text-label');

        }

    });



    $(this).blur(function(){

        if(this.value == '') {

            this.value = $(this).attr('title');

            $(this).addClass('text-label');

        }

    });

    });



    ]]>
    </script>
</body>
</html>
