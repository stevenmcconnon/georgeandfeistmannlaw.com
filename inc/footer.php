<div id="footer">
<p>
Phone: 561-659-6599&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nights: 561-420-3749<br/>
Email: <a href="mailto:ettie@palmbeachtrialgroup.com">ettie@palmbeachtrialgroup.com </a>
</p>
	
	
	<div style="color:#787878; font-size:small; margin-top: 20px; margin-bottom: 20px;">
<p>The Law Offices of George and Feistmann, P.A. assist clients with Personal Injury and Wrongful Death issues, Family Law Matters, Divorce, and Criminal Defense.  We service client from West Palm Beach, Lake Worth, Palm Beach Gardens, Boynton Beach, Jupiter, Delray Beach, Boca Raton in Palm Beach County. We also offer legal representation to clients who reside in Martin, St. Lucie, Okeechobee, Hendry, Broward, and Miami-Dade. </p>
</div>

<p>© 2012 George And Feistmann Law Firm</p>
</div>
