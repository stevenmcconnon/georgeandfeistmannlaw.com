<?php

// to delete a directory use post to the url like 
// http://georgeandfeistmannlaw.com/delete.php?dirname=del
// this will delete all files in a folder called test
//be very carefull! This could delete the entire site!

//makes the full file path
if($_GET['dirname'] != ""){
	$dir = getcwd() . "/" . $_GET['dirname'] . "/";
} else {
	echo "you need to specify a folder name. This script will now exit.<br/>";
	exit();
}


echo "the dirname is <p>$dir</p>... everything inside it will now be deleted!<br/><br/>";


define('PATH', $dir);

destroy(PATH);

function destroy($dir) {
	echo "opening folder...<br/>";
    $mydir = opendir($dir);
    while(false !== ($file = readdir($mydir))) {
        if($file != "." && $file != "..") {
            chmod($dir.$file, 0777);
            if(is_dir($dir.$file)) {
            	echo "deleting $dir $file ...<br/>";
                chdir('.');
                destroy($dir.$file.'/');
                rmdir($dir.$file) or DIE("couldn't delete $dir$file<br />");
            }
            else {
            	echo "deleting $dir $file ...<br/>";
                unlink($dir.$file) or DIE("couldn't delete $dir$file<br />");
                                }
        }
    }
    echo "closing folder...<br/>";
    closedir($mydir);
}

echo 'all done.';
 
 


?>