<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Criminal Defense - Palm Beach</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<style  type="text/css">
  .text-label {
    color: #333;
    font-weight: bold;
}

</style>
<script src="js/jquery.validate.js" type="text/javascript"></script>
<script>
$.validator.addMethod('requiredDefault', function(value, element, param) {
        return value && value != param; // Compare with blank and default (parameter) value
    },
    'Please enter a value.');

$(function() { // Shorthand for $(document).ready(function() {
      $('#reach').validate({
            rules: {
                  input4: { requiredDefault: 'Name' },
                  input: { requiredDefault: 'Email', email: true }
                  
            }
      });
});
</script>
      <style type="text/css">

#reach label.error {
	
	float:right;
	
	clear:both;
	width: auto;
	
	color: #c00;
}

    </style>
	
</head>

<body>
<?php require_once("inc/header.php"); ?>
<div id="wrapper">

  <div id="banner"></div>
  
    
  
   <div id="gold"></div>
  
  <div id="main">
   

  <div class="clearfix"></div>
<?php require_once("inc/sidebar.php"); ?>
   
    <div id="content">
      <h1>Criminal Defense Lawers</h1>
      <br/>
<b>Former Prosecutors</b>

<p>When you are arrested for a criminal offense you need to know your rights.  It is crucial that you hire attorneys who are experienced and knowledgeable, with a thorough understanding of the law.  As former prosecutors with over two decades of combined experience with trials and appeals, we know how to approach a case to get you the best results.  Our prosecutorial experience gave us in-depth understanding of how the police and law enforcement operate.  We can use this knowledge to help you.  <b>Remember the best defense is --knowing the offense.</b></p>

<p><b>We will DEFEND you with the following cases<br/><br/>
	Our team will give you aggressive representation in all areas of criminal law, including:
</b></p>
<ul>
 
<li>DUI / Drunk Driving</li>
<li>Computer-related Crimes</li>
<li>Murder/other violent crimes</li>
<li>Drug Trafficking/Drug Sales</li>
<li>Drug Possession</li>
<li>Robbery, Theft, Burglary</li>
<li>Prescription Fraud</li>
<li>Internet Pornography</li>
<li>Sex Offenses</li>
<li>Assault & Battery</li>
<li>Domestic Battery</li>
<li>Racketeering / Fraud</li>
<li>White-Collar Crimes</li>
<li>Juvenile Matters</li>
<li>Bad Checks</li>
<li>Violations of Probation</li>
<li>Sex Offender Violations</li>
<li>Child Pornography</li>
<li>Organized scheme to defraud</li>
<li>Stalking</li>
<li>All Felonies</li>
<li>All Misdemeanors </li>
<li>Conspiracy Crimes</li>
<li>Solicitation Crimes </li>

</ul>
<br/><br/>
<p>Our Law firm is serving all of Palm Beach County:   Atlantis, Belle Glade, Boca Raton, Boynton Beach, Delray Beach, Greenacres, Jupiter, Lake Worth, Lantana, Loxahatchee, North Palm Beach, Palm Beach, Palm Beach Gardens, Riviera Beach, Royal Palm Beach, Singer Island, Tequesta, Wellington and West Palm Beach.  We also offer legal representation to clients who reside in Martin, St. Lucie, Okeechobee, Hendry, Broward, and Miami-Dade.
</p>



    </div>
    
    
  </div>
</div>

<div class="clearfix"></div>


<?php require_once("inc/footer.php"); ?>

<SCRIPT>

$('input[type="text"]').each(function(){



	this.value = $(this).attr('title');

	$(this).addClass('text-label');



	$(this).focus(function(){

		if(this.value == $(this).attr('title')) {

			this.value = '';

			$(this).removeClass('text-label');

		}

	});



	$(this).blur(function(){

		if(this.value == '') {

			this.value = $(this).attr('title');

			$(this).addClass('text-label');

		}

	});

});



</SCRIPT>
</body>
</html>
