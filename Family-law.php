<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>Criminal Defense - Palm Beach</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery.js">
</script>
    <style type="text/css">
/*<![CDATA[*/
    .text-label {
    color: #333;
    font-weight: bold;
    }



    /*]]>*/
    </style>
    <script src="js/jquery.validate.js" type="text/javascript">
</script>
    <script type="text/javascript">
//<![CDATA[
    $.validator.addMethod('requiredDefault', function(value, element, param) {
        return value && value != param; // Compare with blank and default (parameter) value
    },
    'Please enter a value.');

    $(function() { // Shorthand for $(document).ready(function() {
      $('#reach').validate({
            rules: {
                  input4: { requiredDefault: 'Name' },
                  input: { requiredDefault: 'Email', email: true }
                  
            }
      });
    });
    //]]>
    </script>
    <style type="text/css">
/*<![CDATA[*/

    #reach label.error {

    float:right;

    clear:both;
    width: auto;

    color: #c00;
    }

    /*]]>*/
    </style>
</head>

<body>
    <?php require_once("inc/header.php"); ?>

    <div id="wrapper">
        <div id="banner"></div>

        <div id="gold"></div>

        <div id="main">
            <div class="clearfix"></div><?php require_once("inc/sidebar.php"); ?>

            <div id="content">
                <h1>Family Law Practice</h1><br />

                <h3>We handle the following:</h3><br />

                <ul id="family-law">
                    <li>ALIMONY</li>

                    <li>CHILD CUSTODY (Court ordered parenting classes)</li>

                    <li>STANDARD VISITATION SCHEDULES (in state and out of state)</li>

                    <li>CONTEMPT ISSUES</li>

                    <li>DIVISION OF PROPERTY</li>

                    <li>ENFORCEMENT PROCEEDINGS</li>

                    <li>MEDIATION</li>

                    <li>MODIFICATIONS</li>

                    <li>PSYCHOLOGICAL COUNSELING</li>

                    <li>PRE-MARITAL & POST MARITAL AGREEMENTS</li>

                    <li>TAXATION</li>

                    <li>DISSOLUTION OF MARRIAGE (DIVORCE)<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Includes alimony, child support, custody and equitable distribution issues</li>

                    <li>MODIFICATIONS<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Includes alimony, child support, custody, visitation and relocation</li>

                    <li>DOMESTIC VIOLENCE<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Restraining Orders which may effect child support, custody, and visitation</li>

                    <li>PRE-NUPTIAL AGREEMENTS<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Contract of legal rights/ownership to specified properties not subject to calculation in equitable distribution</li>
                </ul>
<br/><br/>
                <p>Our law firm is dedicated to serve your needs during the litigation period. Our loyal, experienced and dedicated lawyers and staff understand the anxiety involved in litigating a case, and thus they are committed to give you personalized attention and support. With our extensive experience and tenacious devotion we will do everything within our power to give you the best result.</p>
            </div>
        </div>
    </div>

    <div class="clearfix"></div><?php require_once("inc/footer.php"); ?><script type="text/javascript">
//<![CDATA[

    $('input[type="text"]').each(function(){



    this.value = $(this).attr('title');

    $(this).addClass('text-label');



    $(this).focus(function(){

        if(this.value == $(this).attr('title')) {

            this.value = '';

            $(this).removeClass('text-label');

        }

    });



    $(this).blur(function(){

        if(this.value == '') {

            this.value = $(this).attr('title');

            $(this).addClass('text-label');

        }

    });

    });



    //]]>
    </script>
</body>
</html>
